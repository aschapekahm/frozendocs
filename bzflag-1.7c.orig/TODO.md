# to do list for bzflag
---------------------

## 1. Encapsulate stuff requiring platform #ifdef's:
- networking API into libNet.a.
- fork/exec (used in menus.cxx) into libPlatform.a
- file system stuff (dir delimiter, etc.)
_ user name stuff 

## 2. Write bzfbiff
   watch server for players;  run program when somebody joins

## 3. Windows makefiles
   currently have microsoft visual c++ projects only

## 4. Auto-generated dependency support in makefiles
   manual dependencies cause too much trouble

## 5. Smarter robots

## 6. Add type of shot (normal, gm, sw, etc) to killed message

## 7. Support resolutions besides *640x480* on passthrough 3Dfx cards

